use epub::doc::EpubDoc;
use mobi::Mobi;
use std::path::PathBuf;

pub struct Config {
    pub info: BookInfo,
    pub write: bool,
    pub file : String
}

impl Config {
    fn new() -> Config {
        return Config {
            info: BookInfo::new(),
            write: false,
            file : String::new()
        };
    }

    pub fn set_write(&mut self, write: bool) {
        self.write = write;
    }

    pub fn set_file_name(&mut self, file_name : &String) {
        self.file.clone_from(file_name);
    }

    pub fn get_file_name(&self) -> &String {
        return &self.file;
    }

    pub fn from_opts(opts: &getopts::Matches) -> Config {
        let mut cfg = Config::new();

        if opts.opt_present("w") {
            let t = opts.opt_str("w").unwrap();
            cfg.set_write(true);
        }

        if opts.opt_present("t") {
            let t = opts.opt_str("t").unwrap();
            cfg.info.set_title(t);
        }

        if opts.opt_present("a") {
            let t = opts.opt_str("a").unwrap();
            cfg.info.set_author(t);
        }

        if opts.opt_present("i") {
            let t = opts.opt_str("i").unwrap();
            cfg.info.set_isbn(t);
        }

        if opts.opt_present("f") {
            let t = opts.opt_str("f").unwrap();
            cfg.set_file_name(&t);
        }

        return cfg;
    }
}

impl ToString for Config {
    fn to_string(&self) -> String {
        let mut res = String::from("Config\n");
        res.push_str(self.info.to_string().as_str()); 
        return format!("{}", res);
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum BookType {
    NONE,
    TXT,
    EPUB,
    MOBI,
    PDF,
    AZW3,
    DJVU,
    CHM,
}

impl ToString for BookType {
    fn to_string(&self) -> String {
        let res = match self {
            BookType::TXT => "txt",
            BookType::EPUB => "epub",
            BookType::MOBI => "mobi",
            BookType::PDF => "pdf",
            BookType::AZW3 => "azw3",
            BookType::DJVU => "djvu",
            BookType::CHM => "chm",
            _ => "none",
        };

        return format!("{}", res);
    }
}

pub struct BookInfo {
    pub book_type: BookType,
    pub title: String,
    pub author: String,
    pub isbn: String,
}

impl BookInfo {
    pub fn new() -> BookInfo {
        return BookInfo {
            author: String::from(""),
            title: String::from(""),
            isbn: String::from(""),
            book_type: BookType::NONE,
        };
    }

    pub fn set_title(&mut self, s: impl Into<String>) {
        self.title = s.into();
    }

    pub fn set_author(&mut self, s: impl Into<String>) {
        self.author = s.into();
    }

    pub fn set_isbn(&mut self, s: impl Into<String>) {
        self.isbn = s.into();
    }
}

impl ToString for BookInfo {
    fn to_string(&self) -> String {
        let mut res = String::from("title: ");
        res += self.title.as_str();
        res += "\nauthor: ";
        res += self.author.as_str();
        res += "\nisbn: ";
        res += self.isbn.as_str();
        res += "\ntype: ";
        res += self.book_type.to_string().as_str();
        return format!("{}", res);
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ISBN_TYPE {
    NONE,
    TEN,
    THIRTEEN,
}

pub fn is_numeric(s: &String) -> bool {
    for c in s.chars() {
        if !c.is_numeric() {
            return false;
        }
    }
    return true;
}

// VERY ROUGH, no validation, assume digit is already likely isbn
pub fn isbn_type(s: String) -> ISBN_TYPE {
    if s.len() == 10 && is_numeric(&s) {
        return ISBN_TYPE::TEN;
    } else if s.len() == 13 && is_numeric(&s) {
        return ISBN_TYPE::THIRTEEN;
    } else {
        return ISBN_TYPE::NONE;
    }
}

pub fn type_from_extension(ext: &String) -> BookType {
    return match ext.to_lowercase().as_ref() {
        "txt" => BookType::TXT,
        "epub" => BookType::EPUB,
        "mobi" => BookType::MOBI,
        "pdf" => BookType::PDF,
        "azw3" => BookType::AZW3,
        "djvu" => BookType::DJVU,
        "chm" => BookType::CHM,
        _ => BookType::NONE,
    };
}

pub fn load_book(ent: PathBuf, cfg: &Config) -> BookInfo {
    let name = ent
        .file_name()
        .unwrap()
        .to_os_string()
        .into_string()
        .unwrap();

    let parts: Vec<&str> = name.split('.').collect();
    let b: &str = parts.last().unwrap();
    let ext = String::from(b);
    let path = ent.into_os_string().into_string().unwrap();

    let mut ret = BookInfo::new();
    let book_type = type_from_extension(&ext);
    ret.book_type = book_type;

    match book_type {
        BookType::EPUB => {
            let doc = EpubDoc::new(path.to_string());
            match doc {
                Ok(doc) => {
                    if cfg.write {
                        println!("doing write");
                    }

                    let title = doc.mdata("title").unwrap_or_default();
                    ret.set_title(&title);

                    let author = doc.mdata("creator").unwrap_or_default();
                    ret.set_author(&author);

                    let mut isbn = String::from("");
                    for (k, v) in doc.metadata.iter() {
                        if k == "identifier" {
                            for vv in v.iter() {
                                if isbn_type(vv.to_string()) != ISBN_TYPE::NONE {
                                    isbn = String::from(vv);
                                }
                            }
                        }
                    }
                    ret.set_isbn(isbn);
                }
                Err(error) => {
                    println!("{} {}", path, error)
                }
            };
        }
        BookType::MOBI => {
            let doc = Mobi::from_path(path.to_string());
            match doc {
                Ok(doc) => {
                    let title = doc.title().unwrap_or_default();
                    ret.set_title(&title);

                    let author = doc.author().unwrap_or_default();
                    ret.set_author(&author);

                    let isbn = doc.isbn().unwrap_or_default();
                    ret.set_isbn(&isbn);
                }
                Err(error) => {
                    println!("{} {}", path, error)
                }
            };
        }

        BookType::PDF => {
            let mut doc = lopdf::Document::load(path.to_string());
            match doc {
                Ok(doc) => {
                    /*
                    println!("viewing objs");
                    for (k,v) in doc.objects.iter() {
                        println!("XX:{}:{}",k.0, k.1);
                    }
                    let title = String::from("");
                    ret.set_title(&title);
                    */
                    //let author = doc.author().unwrap_or_default();
                    //ret.set_author(&author);

                    //let isbn = doc.isbn().unwrap_or_default();
                    //ret.set_isbn(&isbn);
                }
                Err(error) => {
                    println!("{} {}", path, error)
                }
            };
        }

        _ => {}
    }

    return ret;
}
