use getopts::Options;
use bookedit::{load_book, BookInfo, BookType, Config};
use std::collections::HashMap;
use std::env;
use std::path::PathBuf;
use walkdir::{DirEntry, WalkDir};

fn gather_book(ent: DirEntry, cfg: &Config) {
    let name = ent.file_name().to_str().unwrap();
    let path = ent.path();
    let bk = load_book(path.to_path_buf(), cfg);

    if BookType::NONE != bk.book_type {
        println!("Book {}", bk.to_string());
    }
}

fn walk_dir(dir: &str, cfg: &Config) {
    for entry in WalkDir::new(dir) {
        let en = entry.unwrap();
        let file_type = en.file_type();
        if !file_type.is_dir() {
            gather_book(en, cfg);
        }
    }
}

fn process_book(p: &PathBuf, cfg : &Config) {
    if cfg.write {
        println!("proces_book write mode");
    } else {
        println!("process_book read mode");
        let bk = load_book(p.to_path_buf(), cfg);
        println!("{}", bk.to_string());
    }
}

fn process_file(cfg : &Config) {
    println!("process_file with config \n{}", cfg.info.to_string());

    let path = String::from(cfg.get_file_name());
    let p = PathBuf::from(path);
    process_book(&p, cfg);
}

fn main() {

    let args: Vec<String> = env::args().collect();

    //let program = args[0].clone();
    let mut opts = Options::new();

    opts.optopt("w", "", "write to the file", "WRITE");
    opts.optopt("a", "", "get or set author", "AUTHOR");
    opts.optopt("t", "", "get or set title", "TITLE");
    opts.optopt("i", "", "get or set isbn", "ISBN");
    opts.optopt("f", "", "file to read/write", "FILE");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            panic!(f.to_string())
        }
    };

    let mut cfg = Config::from_opts(&matches);
    process_file(&cfg);
}
